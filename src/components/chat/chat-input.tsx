import { EmojiHappyIcon, PlusIcon } from '@heroicons/react/solid';
import { BaseEmoji, Picker } from 'emoji-mart';
import firebase from 'firebase/compat/app';
import React from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';

import { User } from './types';
import { auth, getDatabase, push, ref, set } from '../../firebase';

function ChatInput() {
  const [messageText, setMessageText] = React.useState('');
  const user: firebase.User = useAuthState(auth)[0];
  const inputRef = React.useRef<null | HTMLInputElement>(null);
  const [showEmojiPicker, setShowEmojiPicker] = React.useState(false);
  const emojiPickerRef = React.useRef<null | HTMLDivElement>(null);

  React.useEffect(() => {
    const checkIfClickedOutside = (e: MouseEvent): void => {
      if (
        showEmojiPicker &&
        !inputRef.current?.contains(e.target as HTMLElement) &&
        !emojiPickerRef.current?.contains(e.target as HTMLDivElement)
      ) {
        setShowEmojiPicker(false);
      }
    };

    document.addEventListener('mousedown', (e) => checkIfClickedOutside(e));

    return () => {
      document.removeEventListener('mousedown', (e) =>
        checkIfClickedOutside(e)
      );
    };
  }, [showEmojiPicker]);

  const sendChatMessage = async () => {
    if (!messageText) return;
    const db = getDatabase();
    const messagesRef = ref(db, 'messages');
    const newMessagesRef = push(messagesRef);
    set(newMessagesRef, {
      message: messageText,
      date: Date.now(),
      user: mapUser(user),
    });
  };

  const clearInputField = (): void => {
    setMessageText('');
  };

  const onSendClick = (): void => {
    sendChatMessage();
    clearInputField();
  };

  const onKeyDown = (e: React.KeyboardEvent): void => {
    if (e.key === 'Enter') {
      sendChatMessage();
      clearInputField();
    }
  };

  const mapUser = (user: firebase.User): User => {
    return {
      name: user.displayName,
      photoURL: user.photoURL,
      id: user.uid,
    };
  };

  const onEmojiIconSelect = (emoji: BaseEmoji): void => {
    addEmojiToMessage(emoji);
    setShowEmojiPicker(false);
  };

  const addEmojiToMessage = (emoji: BaseEmoji): void => {
    const nativeElement = emoji.native;
    const newMessageText = `${messageText}${nativeElement}`;
    setMessageText(newMessageText);
    setTimeout(() => {
      const val = inputRef?.current?.value;
      if (inputRef && inputRef.current && val) {
        inputRef.current.value = '';
        inputRef.current.value = val;
      }
      inputRef?.current?.focus();
    }, 0);
  };

  const toggleEmojiPicker = (): void => {
    setShowEmojiPicker(!showEmojiPicker);
  };

  return (
    <>
      <div className='px-4 fixed bottom-0 py-4 bg-blue-300 w-full flex justify-center items-center'>
        <div className='relative w-full md:w-80 mr-2'>
          <input
            ref={inputRef}
            placeholder='Message...'
            value={messageText}
            onKeyDown={(e) => onKeyDown(e)}
            onChange={(e) => setMessageText(e.target.value)}
            onFocus={function (e) {
              const val = e.target.value;
              e.target.value = '';
              e.target.value = val;
            }}
            className='pr-12 w-full border-none h-12 mr-2 ring-black rounded-md border-gray shadow-lg outline-none focus:ring-2 focus:ring-black'
            type='text'
          />
          <div className='absolute mr-2 h-12 w-8 right-0 bottom-0 flex items-center'>
            <button
              className='w-8 h-8 outline-none focus:ring-2 focus:ring-black rounded-full p-1'
              onClick={toggleEmojiPicker}
            >
              <EmojiHappyIcon color='gray'></EmojiHappyIcon>
            </button>
          </div>
        </div>
        <button
          className='p-2 rounded-full bg-white w-10 h-10 text-black outline-none focus:ring-2 focus:ring-black'
          onClick={onSendClick}
        >
          <PlusIcon>Send</PlusIcon>
        </button>
      </div>
      <div
        ref={emojiPickerRef}
        className='fixed bottom-16 mb-1 w-full md:w-92 px-4 md:px-0'
      >
        {showEmojiPicker && (
          <Picker
            style={{
              width: '100%',
            }}
            onSelect={(emoji) => onEmojiIconSelect(emoji as BaseEmoji)}
          />
        )}
      </div>
    </>
  );
}

export default ChatInput;
