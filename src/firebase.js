import { getDocs, getFirestore } from '@firebase/firestore';
import { initializeApp } from 'firebase/app';
import { getAuth, GoogleAuthProvider, signInWithPopup } from 'firebase/auth';
import { getDatabase, onValue, push, ref, set } from 'firebase/database';
const firebaseConfig = {
  apiKey: process.env.NEXT_PUBLIC_API_KEY,
  authDomain: 'nightcell-chat.firebaseapp.com',
  projectId: 'nightcell-chat',
  storageBucket: 'nightcell-chat.appspot.com',
  messagingSenderId: '791495096647',
  appId: '1:791495096647:web:cf07a391e1c196e2b03462',
  measurementId: 'G-4MD02LQTDL',
};

const app = initializeApp(firebaseConfig);

// Initialize Firebase
const auth = getAuth();
const provider = new GoogleAuthProvider();
const db = getFirestore(app);

export {
  auth,
  db,
  getDatabase,
  getDocs,
  onValue,
  provider,
  push,
  ref,
  set,
  signInWithPopup,
};
