import React from 'react';

interface Props {
  children: JSX.Element;
}

function Header({ children }: Props) {
  return (
    <header className='p-4 items-center flex justify-between fixed w-full top-0 bg-gray-800 z-10'>
      <h1 className='text-white leading-6'>NIghtCell Chat</h1>
      {children}
    </header>
  );
}

export default Header;
