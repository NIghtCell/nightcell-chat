import { remove, update } from '@firebase/database';
import { PencilIcon, XIcon } from '@heroicons/react/solid';
import React from 'react';
import TextareaAutosize from 'react-textarea-autosize';

import { Message } from './types';
import { auth, getDatabase, ref } from '../../firebase';

interface Props {
  message: Message;
}

function ChatMessage({ message: chatMessage }: Props) {
  const [message, setMessage] = React.useState(chatMessage.message);
  const [isInputDisabled, setInputDisabled] = React.useState(true);
  const loggedInUser = auth.currentUser;
  const textAreaRef = React.useRef<null | HTMLTextAreaElement>(null);
  const messageDate = new Date(chatMessage.date);
  const dateTimeString = `${messageDate?.toLocaleDateString()} - ${messageDate?.toLocaleTimeString()}`;
  const user = chatMessage.user;
  const isMessageFromLoggedInUser = loggedInUser?.uid === user.id;
  const userPhotoURL = user ? (user.photoURL as string) : '';

  React.useEffect(() => {
    const checkIfClickedOutside = (e: MouseEvent): void => {
      if (
        !isInputDisabled &&
        textAreaRef.current &&
        !textAreaRef.current.contains(e.target as HTMLElement)
      ) {
        setInputDisabled(true);
        updateChatMessage();
      }
    };

    document.addEventListener('mousedown', (e) => checkIfClickedOutside(e));

    return () => {
      document.removeEventListener('mousedown', (e) =>
        checkIfClickedOutside(e)
      );
    };
  }, [isInputDisabled]);

  const updateChatMessage = async () => {
    if (!message) return;
    const db = getDatabase();
    const messagesRef = ref(db, 'messages/' + chatMessage.id);
    update(messagesRef, {
      ...chatMessage,
      message: message,
    });
  };

  const editMessage = (): void => {
    setInputDisabled(false);
    setTimeout(() => textAreaRef.current?.focus(), 0);
  };

  const deleteMessage = (): void => {
    const db = getDatabase();
    const messagesRef = ref(db, 'messages/' + chatMessage.id);
    remove(messagesRef);
  };

  const onPencilIconClick = (): void => {
    editMessage();
  };

  const onXIconClick = (): void => {
    deleteMessage();
  };

  const onInputKeyDown = (e: React.KeyboardEvent): void => {
    if (e.key === 'Escape' || e.key === 'Enter') {
      setInputDisabled(true);
      updateChatMessage();
    }
  };

  return (
    <div className='flex flex-start mb-4 last:mb-0 w-full md:w-120 bg-white p-4 rounded-lg shadow-lg relative'>
      <img
        className='rounded-full mr-4 h-10 w-10'
        src={userPhotoURL}
        alt='Profile picture'
      />
      <div className='flex flex-col w-full'>
        <span>{user?.name}</span>
        <span className='italic text-sm text-gray-400'>{dateTimeString}</span>
        <TextareaAutosize
          ref={textAreaRef}
          onChange={(e) => setMessage(e.target.value)}
          onKeyDown={(e) => onInputKeyDown(e)}
          disabled={isInputDisabled}
          value={message}
          style={{ wordBreak: 'break-word' }}
          className='break-words border-none outline-none p-0 resize-none focus:ring-0 focus:bg-gray-200'
        ></TextareaAutosize>
      </div>
      {isMessageFromLoggedInUser && (
        <div className='absolute right-0 top-0 p-2'>
          {isInputDisabled && (
            <button
              onClick={onPencilIconClick}
              className='outline-none focus:ring-1 focus:ring-black text-gray-400 w-4 h-4 mr-1 hover:text-black'
            >
              <PencilIcon></PencilIcon>
            </button>
          )}
          <button
            onClick={onXIconClick}
            className='outline-none focus:ring-1 focus:ring-black text-gray-400 w-4 h-4 hover:text-black'
          >
            <XIcon></XIcon>
          </button>
        </div>
      )}
    </div>
  );
}

export default ChatMessage;
