import React from 'react';
import ClipLoader from 'react-spinners/ClipLoader';

import ChatInput from './chat-input';
import ChatMessage from './chat-message';
import { Message } from './types';
import { getDatabase, onValue, ref } from '../../firebase';

function Chat(): JSX.Element {
  const chatRef = React.useRef<null | HTMLDivElement>(null);
  const [messages, setMessages] = React.useState<Message[]>([]);
  const [isLoading, setLoading] = React.useState(true);

  const fetchMessages = async () => {
    const db = getDatabase();
    const messagesRef = ref(db, 'messages');
    onValue(messagesRef, (snapshot) => {
      const newMessages: Message[] = [];
      snapshot.forEach((dataSnapshot) => {
        const dataSnapshotValue = dataSnapshot.val();
        const messageData = {
          ...dataSnapshotValue,
          id: dataSnapshot.key,
        };
        newMessages.push(messageData);
      });
      setMessages(newMessages);
    });
  };

  React.useEffect(() => {
    fetchMessages();
  }, []);

  React.useEffect(() => {
    setLoading(false);
    chatRef?.current?.scrollIntoView(false);
  }, [messages, setLoading]);

  return (
    <div className='flex flex-col items-center p-2 pb-0'>
      {isLoading ? (
        <ClipLoader loading={isLoading} color='white'></ClipLoader>
      ) : (
        <>
          <div className='w-full pb-20 flex flex-col items-center'>
            {messages.map((message: Message, i: number) => (
              <ChatMessage message={message} key={i}></ChatMessage>
            ))}
          </div>
          <div ref={chatRef}></div>
          <ChatInput></ChatInput>
        </>
      )}
    </div>
  );
}

export default Chat;
