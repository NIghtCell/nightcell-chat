import { UserIcon } from '@heroicons/react/solid';
import React from 'react';

import { auth } from '../../firebase';
interface Props {
  onUserIconClick: () => void;
  isUserMenuOpen: boolean;
  userMenuRef: React.RefObject<HTMLDivElement>;
  setUserMenuOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

function UserMenu({
  userMenuRef,
  onUserIconClick,
  isUserMenuOpen,
  setUserMenuOpen,
}: Props) {
  const logoutUser = (): void => {
    auth.signOut();
  };

  const onLogoutClick = (): void => {
    logoutUser();
    closeUserMenu();
  };

  const closeUserMenu = (): void => {
    setUserMenuOpen(false);
  };

  return (
    <div ref={userMenuRef}>
      <button
        onClick={onUserIconClick}
        className='outline-none focus:ring-2 focus:ring-blue-500 h-8 w-8 ml-auto rounded-full bg-white p-1 block shadow-lg'
      >
        <UserIcon></UserIcon>
      </button>
      {isUserMenuOpen && (
        <div className='md:mt-2 mt-1 bg-white p-2 shadow-lg rounded-md w-full md:w-32 absolute right-0 text-center'>
          <button onClick={onLogoutClick}>Logout</button>
        </div>
      )}
    </div>
  );
}

export default UserMenu;
