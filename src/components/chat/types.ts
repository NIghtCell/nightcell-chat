export interface User {
  id: string | null;
  name: string | null;
  photoURL: string | null;
}

export interface Message {
  id: string;
  message: string;
  date: Date;
  user: User;
}
