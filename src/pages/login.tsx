import React from 'react';

import { auth, provider, signInWithPopup } from '../firebase';

export default function Login() {
  const loginWithGoogle = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ): void => {
    e.preventDefault();
    signInWithPopup(auth, provider);
  };
  const onLoginWithGoogleClick = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ): void => {
    loginWithGoogle(e);
  };
  return (
    <div className='w-full h-full flex items-center justify-center'>
      <div className='flex justify-center flex-col'>
        <h1 className='mb-2 flex justify-center'>Login</h1>
        <button
          onClick={(e) => onLoginWithGoogleClick(e)}
          className='bg-red-800 hover:bg-white hover:text-red-800 text-white rounded-lg p-2'
        >
          Login with Google
        </button>
      </div>
    </div>
  );
}
