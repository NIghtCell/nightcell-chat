import firebase from 'firebase/compat/app';
import * as React from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';

import Header from '@/components/header/header';
import UserMenu from '@/components/user-menu/user-menu';

import Home from './home';
import Login from './login';
import { auth } from '../firebase';

export default function HomePage() {
  const userMenuRef = React.useRef<HTMLDivElement>(null);
  const user: firebase.User = useAuthState(auth)[0];
  const [isUserMenuOpen, setUserMenuOpen] = React.useState(false);
  const onUserIconClick = () => {
    toggleUserMenu();
  };

  const toggleUserMenu = (): void => {
    setUserMenuOpen(!isUserMenuOpen);
  };

  const onMainClick = (e: React.MouseEvent): void => {
    closeUserMenuOnClickOutisde(e);
  };

  const closeUserMenuOnClickOutisde = (e: React.MouseEvent): void => {
    if (
      userMenuRef.current &&
      !userMenuRef.current.contains(e.target as HTMLElement)
    ) {
      setUserMenuOpen(false);
    }
  };

  return (
    <>
      <Header>
        {user && (
          <UserMenu
            onUserIconClick={onUserIconClick}
            userMenuRef={userMenuRef}
            isUserMenuOpen={isUserMenuOpen}
            setUserMenuOpen={setUserMenuOpen}
          ></UserMenu>
        )}
      </Header>
      <main className='pt-16' onClick={(e) => onMainClick(e)}>
        <div>{!user ? <Login></Login> : <Home></Home>}</div>
      </main>
    </>
  );
}
