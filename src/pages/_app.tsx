import { AppProps } from 'next/app';

import '@/styles/globals.css';
import 'emoji-mart/css/emoji-mart.css';

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />;
}

export default MyApp;
